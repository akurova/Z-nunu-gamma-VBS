#include "ZnunuGamVBS_pack/ZnunuGamVBS.h"
#include "HGamAnalysisFramework/TruthUtils.h"
#include "HGamAnalysisFramework/HGamCommon.h"
#include "HGamAnalysisFramework/HGamVariables.h"


void ZnunuGamVBS::truthLevel()
{
  // truth study

  //declare truth container

  const xAOD::TruthParticleContainer* TruthP = 0;
  if (!event()->retrieve(TruthP, "TruthParticles").isSuccess() ){ // retrieve arguments: container type, container key
    HG::fatal("Failed to retrieve TruthParticleContainer container. Exiting.");
  }


  //////true photons
  xAOD::TruthParticleContainer truthPhotons = truthHandler()->getPhotons();
  xAOD::TruthParticleContainer sel_truthPhotons= truthHandler()->applyPhotonSelection(truthPhotons);
  xAOD::TruthParticleContainer sel_tr_ph(SG::VIEW_ELEMENTS);
  double pt=0;
  double tr_etcone40=-10;
  double tr_ptcone20=-10;
  lead_ph=TLorentzVector();
  vec=TLorentzVector();

  static std::vector<int> ignorePdgIds = { 13, 12, 14, 16, 18 }; // mu, nus

  for (auto ph: sel_truthPhotons){  //looking for leading photon

    for (unsigned int i = 0; i < ph->nParents(); i++){
      if ( ph->parent(i)->pdgId()==22 && ph->barcode() < 200000){  // phot parent has the same pdgID, meaning it comes from Z. It is how it works in Sherpa, not for Pythia! 
        tr_ph_n=tr_ph_n+1;
        vec = ph->p4();
        sel_tr_ph.push_back(ph);
        if (vec.Pt() > pt){
          pt = vec.Pt();
          lead_ph = vec;
          /// extract truth iso
          tr_etcone40 = HG::getTruthIsolation(ph, TruthP, 0.4, false, ignorePdgIds)*HG::invGeV;
          tr_ptcone20 = HG::getTruthIsolation(ph, TruthP, 0.2, true, {}, 1.0 * HG::GeV)*HG::invGeV;
          /////
        }
      }
    }
  }

  lead_ph *= HG::invGeV; // convert these 4-vectors to GeV
  tr_ph_pt = lead_ph.Pt();
  tr_ph_eta = lead_ph.Eta();
  tr_ph_phi = lead_ph.Phi();
  tr_ph_etcone40=tr_etcone40;
  tr_ph_ptcone20=tr_ptcone20;

//////true neutrino
  vec=TLorentzVector();
  pt=0;
  int lead_neutr_pdgId=0;
  int sublead_neutr_pdgId=0;
  vec=sublead_neutr=lead_neutr=neutr_syst=TLorentzVector();

  for (auto neutr: *TruthP) {
    if (neutr->nParents()==0) continue;

    ///check neutrino parent
    if ((fabs(neutr->pdgId())==12 || fabs(neutr->pdgId())==14 || fabs(neutr->pdgId())==16) && neutr->status()==1 && neutr->barcode() < 200000){//there could be another status for Pythia!

      for (unsigned int i = 0; i < neutr->nParents(); i++){
        if ( neutr->parent(i)->pdgId()==neutr->pdgId() || neutr->parent(i)->pdgId()==23 ){   // neutrino parent has the same pdgID, meaning it comes from Z. It is how it works in Sherpa, not for Pythia!
          vec = neutr->p4();
          if (vec.Pt() > pt){
            pt = vec.Pt();
            lead_neutr = vec;
            lead_neutr_pdgId=neutr->pdgId();
          }
        }
      }
    }
  }
  pt=0;
  vec=TLorentzVector();
  for (auto ne: *TruthP){
    if (ne->nParents()==0) continue;

    ///check neutrino parent
    if ((fabs(ne->pdgId())==12 || fabs(ne->pdgId())==14 || fabs(ne->pdgId())==16) && ne->status()==1 && ne->barcode() < 200000){
      for (unsigned int i = 0; i < ne->nParents(); i++){
        if ( ne->parent(i)->pdgId()==ne->pdgId() || ne->parent(i)->pdgId()==23 ){   // neutrino parent has the same pdgID, meaning it comes from Z. It is how it works in Sherpa
          vec = ne->p4();
          if (vec!=lead_neutr && vec.Pt()>pt){
            pt = vec.Pt();
            sublead_neutr = vec;
            sublead_neutr_pdgId=ne->pdgId();
          }
        }
      }
    }
  }

  if  (lead_neutr_pdgId+sublead_neutr_pdgId==0){
    neutr_syst = sublead_neutr+lead_neutr;
    neutr_syst *= HG::invGeV; // convert these 4-vectors to GeV
    tr_Z_pt = neutr_syst.Pt();
    tr_Z_phi = neutr_syst.Phi();
    tr_Z_n = 1;
  }

  //////true elec
  xAOD::TruthParticleContainer truthElectrons = truthHandler()->getElectrons();
  histoStore()->fillTH1F("n_el", truthElectrons.size()); 
  xAOD::TruthParticleContainer sel_truthElectrons= truthHandler()->applyElectronSelection(truthElectrons);
  histoStore()->fillTH1F("n_el1", sel_truthElectrons.size()); 

  xAOD::TruthParticleContainer sel_tr_el(SG::VIEW_ELEMENTS);
  vec=TLorentzVector();
  for (auto el: sel_truthElectrons){
    vec = el->p4();
    if (el->status()==1 && el->barcode() < 200000) {
      sel_tr_el.push_back(el);
      tr_el_n++;      
    }
  }
                                
  //////true muon
  xAOD::TruthParticleContainer truthMuons = truthHandler()->getMuons();
  xAOD::TruthParticleContainer sel_truthMuons= truthHandler()->applyMuonSelection(truthMuons);
  xAOD::TruthParticleContainer sel_tr_mu(SG::VIEW_ELEMENTS);
  vec=TLorentzVector();
  for (auto mu: sel_truthMuons){
    vec = mu->p4();
    if (mu->status()==1 && mu->barcode() < 200000) {
      sel_tr_mu.push_back(mu);
      tr_mu_n++;
    }
  }

  //////true jets
  xAOD::JetContainer truthJets = truthHandler()->getJets();
  xAOD::JetContainer sel_truthJets= truthHandler()->applyJetSelection(truthJets); 
  xAOD::JetContainer sel_tr_jet(SG::VIEW_ELEMENTS);

  /// overlap with photons 0.3
  for (auto jet: sel_truthJets){   
    if(HG::minDR(jet, sel_tr_ph) < 0.3) continue;
    sel_tr_jet.push_back(jet);
  }
  pt=0; tr_jet_sum_pt=0;
  vec=TLorentzVector();
  tr_lead_jet=TLorentzVector();
  tr_slead_jet=TLorentzVector();

  for (auto jet_itr: sel_tr_jet){      //looking for leading jet
    tr_jet_sum_pt=(tr_jet_sum_pt+jet_itr->p4().Pt());
    vec = jet_itr->p4();
    if (vec.Pt() > pt){
      tr_slead_jet = vec;
      if (tr_slead_jet.Pt()>tr_lead_jet.Pt()){
        tr_slead_jet=tr_lead_jet;
        tr_lead_jet=vec;
      }
      pt=tr_slead_jet.Pt();
    }
  }

  tr_jet_sum_pt *= HG::invGeV;
  tr_lead_jet *= HG::invGeV; // convert these 4-vectors to GeV
  tr_slead_jet *= HG::invGeV; // convert these 4-vectors to GeV
  tr_jet_n = sel_tr_jet.size();
  tr_jet_pt = tr_lead_jet.Pt();
  tr_jet_eta = tr_lead_jet.Eta();
  tr_jet_phi = tr_lead_jet.Phi();
  tr_jet_E = tr_lead_jet.E();
  tr_sl_jet_pt = tr_slead_jet.Pt();
  tr_sl_jet_eta = tr_slead_jet.Eta();
  tr_sl_jet_phi = tr_slead_jet.Phi();
  tr_sl_jet_E = tr_slead_jet.E();

  //fill tree
  if (tr_ph_n!=0 && tr_Z_n==1 && sublead_neutr_pdgId!=0 && tr_Z_pt>150){
    m_tr_tree->Fill();
  }

}