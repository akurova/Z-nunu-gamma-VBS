#pragma once

// Local include(s):
#include "HGamAnalysisFramework/HgammaAnalysis.h"


//! \brief Hgamma namespace
namespace HG {

  //! \name   A few general helper methods and definitions
  //! \author Dag Gillberg
  //@{

  void MyRunJob(HgammaAnalysis *alg, int argc, char **argv);

  //@}
}